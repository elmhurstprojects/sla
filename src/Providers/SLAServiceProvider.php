<?php namespace ElmhurstProjects\SLA\Providers;

use Illuminate\Support\ServiceProvider;

class SLAServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/sla.php' => config_path('sla.php')
        ], 'config');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
