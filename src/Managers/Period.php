<?php namespace ElmhurstProjects\SLA\Managers;

use Carbon\Carbon;

class Period
{
    public $first_day;

    public $last_day;

    public $middle_days = [];

    protected $work_start_time;

    protected $work_end_time;

    protected $work_days;

    protected $recurring_non_working_days;

    protected $adhoc_non_working_days;

    public function __construct()
    {
        $this->first_day = Carbon::now();

        $this->last_day = Carbon::now();

        $this->work_start_time = config('sla.work_start_time');

        $this->work_end_time = config('sla.work_end_time');

        $this->work_days = config('sla.work_days');

        $this->recurring_non_working_days = config('sla.recurring_non_working_days');

        $this->adhoc_non_working_days = config('sla.adhoc_non_working_days');
    }

    /**
     * Set the object main parameters, essential to get class to work
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return mixed
     */
    public function setDays(Carbon $start_date, Carbon $end_date)
    {
        $this->setFirstDay($start_date)->setLastDay($end_date)->setMiddleDays($start_date, $end_date);

        return $this;
    }

    /**
     * Returns the minutes for the set period
     * @return int
     */
    public function getMinutes():int
    {
        if($this->first_day->format('Y-m-d') == $this->last_day->format('Y-m-d')){
            return $this->first_day->diffInMinutes($this->last_day);
        }else{
            $first_day_minutes = $this->first_day->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i', $this->first_day->format('Y-m-d') . $this->work_end_time));

            $last_day_minutes = Carbon::createFromFormat('Y-m-d H:i', $this->last_day->format('Y-m-d') . $this->work_start_time)->diffInMinutes($this->last_day);

            $middle_day_minutes = count($this->middle_days) * $this->getMinutesInFullWorkingDay();

            return $first_day_minutes + $middle_day_minutes + $last_day_minutes;
        }
    }

    /**
     * Sets the work start time
     * @param string $work_start_time
     * @return $this
     */
    public function setWorkStartTime(string $work_start_time)
    {
        $this->work_start_time = $work_start_time;

        return $this;
    }

    /**
     * Sets the work end time
     * @param string $work_end_time
     * @return $this
     */
    public function setWorkEndTime(string $work_end_time)
    {
        $this->work_end_time = $work_end_time;

        return $this;
    }

    /**
     * Sets the recurring non working days such as Christmas, Boxing Day
     * @param array $recurring_non_working_days ['MM-DD', 'MM-DD']
     * @return $this
     */
    public function setRecurringNonWorkingDays(array $recurring_non_working_days)
    {
        $this->recurring_non_working_days = $recurring_non_working_days;

        return $this;
    }

    /**
     * Sets the adhoc non working days such as Office Closure
     * @param array $adhoc_non_working_days ['YYYY-MM-DD', 'YYYY-MM-DD']
     * @return $this
     */
    public function setAdhocNonWorkingDays(array $adhoc_non_working_days)
    {
        $this->adhoc_non_working_days = $adhoc_non_working_days;

        return $this;
    }

    /**
     * Sets the adhoc non working days such as Office Closure
     * @param string $recurring_non_working_day 'MM-DD'
     * @return $this
     */
    public function addRecurringNonWorkingDays(string $recurring_non_working_day)
    {
        $this->recurring_non_working_days[] = $recurring_non_working_day;

        return $this;
    }

    /**
     * Sets the adhoc non working days such as Office Closure
     * @param string $adhoc_non_working_day 'YYYY-MM-DD'
     * @return $this
     */
    public function addAdhocNonWorkingDays(string $adhoc_non_working_day)
    {
        $this->adhoc_non_working_days[] = $adhoc_non_working_day;

        return $this;
    }

    /**
     * Gets the work start time in HH:MM
     * @return string
     */
    public function getWorkStartTime():string
    {
        return $this->work_start_time;
    }

    /**
     * Gets the work end time in HH:MM
     * @return string
     */
    public function getWorkEndTime():string
    {
        return $this->work_end_time;
    }

    /**
     * Sets the first day
     * @param Carbon $first_day
     * @return $this
     */
    protected function setFirstDay(Carbon $first_day)
    {
        $this->first_day = $first_day;

        return $this;
    }

    /**
     * Sets the last day
     * @param Carbon $last_day
     * @return $this
     */
    protected function setLastDay(Carbon $last_day)
    {
        $this->last_day = $last_day;

        return $this;
    }

    /**
     * Set the days between start and end
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return $this
     */
    protected function setMiddleDays(Carbon $start_date, Carbon $end_date)
    {
        $cycle_date = $start_date->copy()->addDay();

        while ($cycle_date->format('Y-m-d 00:00:00') < $end_date->format('Y-m-d 00:00:00')) {
            if($this->isWorkDay($cycle_date)) $this->middle_days[] = $cycle_date->format('Y-m-d');

            $cycle_date->addDay();
        }

        return $this;
    }

    /**
     * This works out if the day is a usual business working day
     * @param Carbon $day
     * @return bool
     */
    public function isWorkDay(Carbon $day)
    {
        if(!in_array($day->format('D'), $this->work_days)) return false;

        if(in_array($day->format('m-d'), $this->recurring_non_working_days)) return false;

        if(in_array($day->format('Y-m-d'), $this->adhoc_non_working_days)) return false;

        return true;
    }

    /**
     * Gets the minutes in a full working day
     * @return int
     */
    protected function getMinutesInFullWorkingDay():int
    {
        return Carbon::createFromFormat('H:i', $this->work_start_time)->diffInMinutes(Carbon::createFromFormat('H:i', $this->work_end_time));
    }
}
