<?php namespace ElmhurstProjects\SLA\Managers;

use Carbon\Carbon;

class SLAManager
{
    protected $period;

    public function __construct()
    {
        $this->period = new Period();
    }

    /**
     * Returns the amount of minutes during period, ie 234
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return int
     */
    public function SLAMinutesDuringPeriod(Carbon $start_date, Carbon $end_date):? int
    {
        if ($end_date < $start_date) return null;

        $this->period->setDays($start_date, $end_date);

        return $this->period->getMinutes();
    }

    /**
     * Returns the amount of hours during period, ie 3.78
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return float
     */
    public function SLAHoursDuringPeriod(Carbon $start_date, Carbon $end_date):? float
    {
        if ($end_date < $start_date) return null;

        $this->period->setDays($start_date, $end_date);

        return round($this->period->getMinutes() / 60, 2);
    }

    /**
     * Returns the amount of time during period h:m, ie 9:32
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return string
     */
    public function SLATimeDuringPeriod(Carbon $start_date, Carbon $end_date):? string
    {
        if ($end_date < $start_date) return null;

        $this->period->setDays($start_date, $end_date);

        $time = round($this->period->getMinutes() / 60, 2);

        return floor($time) . ':' . (floor($time * 60) % 60);
    }

    /**
     * Returns weather or not date is during business hours
     * @param Carbon $date
     * @return bool
     */
    public function dateDuringBusinessHours(Carbon $date):bool
    {
        if(!$this->period->isWorkDay($date)) return false;

        return ($date->format('H:i') >= $this->period->getWorkStartTime() && $date->format('H:i') <= $this->period->getWorkEndTime());
    }
}
