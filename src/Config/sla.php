<?php

return [
    //The time of day the SLA starts for each day HH:MM
    'work_start_time' => "08:30",

    //The time of day the SLA ends for each day HH:MM
    'work_end_time' => "17:30",

    //The days of the week the SLA is applied
    'work_days' => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'],

    //Recurring dates every year to be omitted from SLA time, ie Christmas, Boxing Day
    'recurring_non_working_days' => ['12-25', '12-26'],

    //One of dates that should be excluded from SLA time, ie Office closure
    'adhoc_non_working_days' => ['2019-12-25']
];