# SLA Manager

> Laravel manager class to calculate the SLA time between two Carbon periods, taking into account work hours per day, weekends and Holiday days

---

## Installation

```shell
composer require elmhurstprojects/sla
```

### Setup

- Now publish the config so you can set your own SLA times or just use the defaults. Found in sla.php

```shell
php artisan vendor:publish --tag=config
```

## Usage 
```shell
 $this->sla_manager = new SLAManager();
 
 $this->sla_manager->SLAMinutesDuringPeriod(Carbon::now()->subHour(), Carbon::now())
 
 $this->sla_manager->SLAHoursDuringPeriod(Carbon::now()->subHours(3), Carbon::now())
 
 $this->sla_manager->SLATimeDuringPeriod(Carbon::now()->subDays(2), Carbon::now())
```